defmodule Recurse do
  def mapsum([head | tail], fun), do: mapsum(tail, fun, head)

  defp mapsum([], _fun, memo), do: memo

  defp mapsum([head | tail], fun, memo) do
    sum = fun.(head) + memo
    mapsum(tail, fun, sum)
  end
end
