defmodule HiLo do
  def guess(actual, lo..hi) do
    guess(actual, lo..hi, calcGuess(lo, hi))
  end

  defp guess(actual, _, actual), do: actual

  defp guess(actual, _lo..hi, guessed) when actual > guessed do
    guess(actual, guessed..hi, calcGuess(guessed, hi))
  end

  defp guess(actual, lo.._hi, guessed) when actual < guessed do
    guess(actual, lo..guessed, calcGuess(lo, guessed))
  end

  defp calcGuess(lo, hi) do
    result = lo + div(hi-lo, 2)
    IO.puts "Is it #{result}"
    result
  end
end
